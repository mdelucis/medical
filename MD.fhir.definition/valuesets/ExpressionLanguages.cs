﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class ExpressionLanguages
    {
        public static List<KeyValuePair<string, string>> language = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("text/cql", "CQL"),
            new KeyValuePair<string, string>("text/fhirpath", "FHIRPath"),
            new KeyValuePair<string, string>("application/x-fhir-query", "FHIR Query"),            
        };
    }
}
