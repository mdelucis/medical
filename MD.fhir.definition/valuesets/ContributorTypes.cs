﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class ContributorTypes
    {
        public static List<KeyValuePair<string, string>> type = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("author", "Author"),
            new KeyValuePair<string, string>("editor", "Editor"),
            new KeyValuePair<string, string>("reviewer", "Reviewer"),
            new KeyValuePair<string, string>("endorser", "Endorser")
        };
    }
}
