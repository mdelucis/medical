﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class MaritalStatuses
    {
        public static List<KeyValuePair<string, string>> status = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("A", "Annulled"),
            new KeyValuePair<string, string>("D", "Divorced"),
            new KeyValuePair<string, string>("I", "Interlocutory"),
            new KeyValuePair<string, string>("L", "Legally Separated"),
            new KeyValuePair<string, string>("M", "Married"),

            new KeyValuePair<string, string>("P", "Polygamous"),
            new KeyValuePair<string, string>("S", "Never Married"),
            new KeyValuePair<string, string>("T", "Domestic Partner"),
            new KeyValuePair<string, string>("U", "Unmarried"),
            new KeyValuePair<string, string>("W", "Widowed"),
            new KeyValuePair<string, string>("UNK", "Unknown")
        };
    }
}
