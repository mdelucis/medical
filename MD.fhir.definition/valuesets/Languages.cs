﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class Languages
    {
        public static List<KeyValuePair<string, string>> language = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("ar", "Arabic"),
            new KeyValuePair<string, string>("bn", "Bengali"),
            new KeyValuePair<string, string>("cs", "Czech"),
            new KeyValuePair<string, string>("da", "Danish"),
            new KeyValuePair<string, string>("de", "German"),
            new KeyValuePair<string, string>("de-AT", "German (Austria)"),
            new KeyValuePair<string, string>("de-CH", "German (Switzerland)"),
            new KeyValuePair<string, string>("de-DE", "German (Germany)"),
            new KeyValuePair<string, string>("el", "Greek"),
            new KeyValuePair<string, string>("en", "English"),
            new KeyValuePair<string, string>("en-AU", "English (Australia)"),
            new KeyValuePair<string, string>("en-CA", "English (Canada)"),
            new KeyValuePair<string, string>("en-GB", "English (Great Britain)"),
            new KeyValuePair<string, string>("en-IN", "English (India)"),
            new KeyValuePair<string, string>("en-NZ", "English (New Zeland)"),
            new KeyValuePair<string, string>("en-SG", "English (Singapore)"),
            new KeyValuePair<string, string>("en-US", "English (United States)"),
            new KeyValuePair<string, string>("es", "Spanish"),
            new KeyValuePair<string, string>("es-AR", "Spanish (Argentina)"),
            new KeyValuePair<string, string>("es-ES", "Spanish (Spain)"),
            new KeyValuePair<string, string>("es-UY", "Spanish (Uruguay)"),
            new KeyValuePair<string, string>("fi", "Finnish"),
            new KeyValuePair<string, string>("fr", "French"),
            new KeyValuePair<string, string>("fr-BE", "French (Belgium)"),
            new KeyValuePair<string, string>("fr-CH", "French (Switzerland)"),
            new KeyValuePair<string, string>("fr-FR", "French (France)"),
            new KeyValuePair<string, string>("fy", "Frysian"),
            new KeyValuePair<string, string>("fy-NL", "Frysian (Netherlands)"),
            new KeyValuePair<string, string>("hi", "Hindi"),
            new KeyValuePair<string, string>("hr", "Croatian"),
            new KeyValuePair<string, string>("it", "Italian"),
            new KeyValuePair<string, string>("it-CH", "Italian (Switzerland)"),
            new KeyValuePair<string, string>("it-IT", "Italian (Italy)"),
            new KeyValuePair<string, string>("ja", "Japanese"),
            new KeyValuePair<string, string>("ko", "Korean"),
            new KeyValuePair<string, string>("nl", "Dutch"),
            new KeyValuePair<string, string>("nl-BE", "Dutch (Belgium)"),
            new KeyValuePair<string, string>("nl-NL", "Dutch (Netherlands)"),
            new KeyValuePair<string, string>("no", "Norwegian"),
            new KeyValuePair<string, string>("no-NO", "Norwegian (Norway)"),
            new KeyValuePair<string, string>("pa", "Punjabi"),
            new KeyValuePair<string, string>("pl", "Polish"),
            new KeyValuePair<string, string>("pt", "Portuguese"),
            new KeyValuePair<string, string>("pt-BR", "Portuguese (Brazil)"),
            new KeyValuePair<string, string>("ru", "Russian"),
            new KeyValuePair<string, string>("ru-RU", "Russian (Russia)"),
            new KeyValuePair<string, string>("sr", "Serbian"),
            new KeyValuePair<string, string>("sr-RS", "Serbian (Serbia)"),
            new KeyValuePair<string, string>("sv", "Swedish"),
            new KeyValuePair<string, string>("sv-SE", "Swedish (Sweden)"),
            new KeyValuePair<string, string>("te", "Telegu"),
            new KeyValuePair<string, string>("zh", "Chinese"),
            new KeyValuePair<string, string>("zh-CN", "Chinese (China)"),
            new KeyValuePair<string, string>("zh-HK", "Chinese (Hong Kong)"),
            new KeyValuePair<string, string>("zh-SG", "Chinese (Singapore)"),
            new KeyValuePair<string, string>("zh-TW", "Chinese (Taiwan)")
        };
    }
}
