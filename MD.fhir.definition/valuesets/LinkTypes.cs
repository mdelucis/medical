﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class LinkTypes
    {
        public static List<KeyValuePair<string, string>> type = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("replaced-by", "Replaced-by"),
            new KeyValuePair<string, string>("replaces", "Replaces"),
            new KeyValuePair<string, string>("refer", "Refer"),
            new KeyValuePair<string, string>("seealso", "See Also")
        };
    }
}
