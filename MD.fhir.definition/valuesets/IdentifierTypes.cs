﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class IdentifierTypes
    {
        public static List<KeyValuePair<string, string>> type = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("DL", "Driver's license number"),
            new KeyValuePair<string, string>("PPN", "Passport number"),
            new KeyValuePair<string, string>("BRN", "Breed Registry Number"),
            new KeyValuePair<string, string>("MR", "Medical record number"),
            new KeyValuePair<string, string>("MCN", "Microchip Number"),

            new KeyValuePair<string, string>("EN", "Employer number"),
            new KeyValuePair<string, string>("TAX", "Tax ID number"),
            new KeyValuePair<string, string>("NIIP", "National Insurance Payor Identifier (Payor)"),
            new KeyValuePair<string, string>("PRN", "Provider number"),
            new KeyValuePair<string, string>("MD", "Medical License number"),

            new KeyValuePair<string, string>("DR", "Donor Registration Number"),
            new KeyValuePair<string, string>("ACSN", "Accession ID"),
            new KeyValuePair<string, string>("UDI", "Universal Device Identifier"),
            new KeyValuePair<string, string>("SNO", "Serial Number"),
            new KeyValuePair<string, string>("SB", "Social Beneficiary Identifier"),

            new KeyValuePair<string, string>("PLAC", "Placer Identifier	"),
            new KeyValuePair<string, string>("FILL", "Filler Identifier	"),
            new KeyValuePair<string, string>("JHN", "Jurisdictional health number (Canada)")
        };
    }
}
