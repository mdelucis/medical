﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class ContactPointSystems
    {
        public static List<KeyValuePair<string, string>> system = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("phone", "Phone"),
            new KeyValuePair<string, string>("fax", "Fax"),
            new KeyValuePair<string, string>("email", "Email"),
            new KeyValuePair<string, string>("pager", "Pager"),
            new KeyValuePair<string, string>("url", "URL"),
            new KeyValuePair<string, string>("sms", "SMS"),
            new KeyValuePair<string, string>("other", "Other")
        };
    }
}
