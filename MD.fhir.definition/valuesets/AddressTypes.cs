﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class AddressTypes
    {
        public static List<KeyValuePair<string, string>> type = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("postal", "Postal"),
            new KeyValuePair<string, string>("physical", "Physical"),
            new KeyValuePair<string, string>("both", "Postal & Physical")
        };
    }
}
