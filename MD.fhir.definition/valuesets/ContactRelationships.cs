﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class ContactRelationships
    {
        public static List<KeyValuePair<string, string>> relationship = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("C", "Emergency Contact"),
            new KeyValuePair<string, string>("E", "Employer"),
            new KeyValuePair<string, string>("F", "Federal Agency"),
            new KeyValuePair<string, string>("I", "Insurance Company"),
            new KeyValuePair<string, string>("N", "Next-of-Kin"),

            new KeyValuePair<string, string>("S", "State Agency"),
            new KeyValuePair<string, string>("U", "Unknown")
        };
    }
}
