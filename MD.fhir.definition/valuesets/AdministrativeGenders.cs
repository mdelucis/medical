﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class AdministrativeGenders
    {
        public static List<KeyValuePair<string, string>> gender = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("male", "Male"),
            new KeyValuePair<string, string>("female", "Female"),
            new KeyValuePair<string, string>("other", "Other"),
            new KeyValuePair<string, string>("unknown", "Unknown")
        };
    }
}
