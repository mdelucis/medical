﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class UsageContextTypes
    {
        public static List<KeyValuePair<string, string>> type = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("gender", "Gender"),
            new KeyValuePair<string, string>("age", "Age Range"),
            new KeyValuePair<string, string>("focus", "Clinical Focus"),
            new KeyValuePair<string, string>("user", "User Type"),
            new KeyValuePair<string, string>("workflow", "Workflow Setting"),

            new KeyValuePair<string, string>("task", "Workflow Task"),
            new KeyValuePair<string, string>("venue", "Clinical Venue"),
            new KeyValuePair<string, string>("species", "Species"),
            new KeyValuePair<string, string>("program", "Program")
        };
    }
}
