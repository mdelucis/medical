﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class QuantityComparators
    {
        public static List<KeyValuePair<string, string>> comparator = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("<", "Less than"),
            new KeyValuePair<string, string>("<=", "Less or Equal to"),
            new KeyValuePair<string, string>(">=", "Greater or Equal to"),
            new KeyValuePair<string, string>(">", "Greater than"),          
        };
    }
}
