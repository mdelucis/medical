﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class AddressUses
    {
        public static List<KeyValuePair<string, string>> use = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("home", "Home"),
            new KeyValuePair<string, string>("work", "Work"),
            new KeyValuePair<string, string>("temp", "Temporary"),            
            new KeyValuePair<string, string>("old", "Old"),
            new KeyValuePair<string, string>("billing", "Billing")
        };
    }
}
