﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class Qualifications
    {
        public static List<KeyValuePair<string, string>> qualification = new List<KeyValuePair<string, string>>();

        static Qualifications()
        {
            using (StreamReader r = new StreamReader("valuesets/qualifications.json"))
            {
                string json = r.ReadToEnd();
                Dictionary<string, string> items = JsonConvert.DeserializeObject<Dictionary<string, string>>(json);

                foreach (var item in items)
                {
                    qualification.Add(new KeyValuePair<string, string>(item.Key, item.Value));
                }
            }
        }       
    }
}
