﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class NameUses
    {
        public static List<KeyValuePair<string, string>> use = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("usual", "Usual"),
            new KeyValuePair<string, string>("official", "Official"),
            new KeyValuePair<string, string>("temp", "Temp"),
            new KeyValuePair<string, string>("nickname", "Nickname"),
            new KeyValuePair<string, string>("anonymous", "Anonymous"),            
            new KeyValuePair<string, string>("old", "Old"),
            new KeyValuePair<string, string>("maiden", "Maiden")
        };
    }
}
