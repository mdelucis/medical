﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class IdentifierUses
    {
        public static List<KeyValuePair<string, string>> use = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("usual", "Usual"),
            new KeyValuePair<string, string>("official", "Official"),
            new KeyValuePair<string, string>("temp", "Temp"),
            new KeyValuePair<string, string>("secondary", "Secondary"),
            new KeyValuePair<string, string>("old", "Old")
        };
    }
}
