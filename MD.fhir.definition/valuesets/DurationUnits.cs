﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class DurationUnits
    {
        public static List<KeyValuePair<string, string>> unit = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("ms", "milliseconds"),
            new KeyValuePair<string, string>("s", "seconds"),
            new KeyValuePair<string, string>("min", "minutes"),
            new KeyValuePair<string, string>("h", "hours"),
            new KeyValuePair<string, string>("d", "days"),
            new KeyValuePair<string, string>("wk", "weeks"),
            new KeyValuePair<string, string>("mo", "months"),
            new KeyValuePair<string, string>("a", "years")
        };
    }
}
