﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.valuesets
{
    public static class ContactPointUses
    {
        public static List<KeyValuePair<string, string>> use = new List<KeyValuePair<string, string>>()
        {
            new KeyValuePair<string, string>("home", "Home"),
            new KeyValuePair<string, string>("work", "Work"),
            new KeyValuePair<string, string>("temp", "Temp"),            
            new KeyValuePair<string, string>("old", "Old"),
            new KeyValuePair<string, string>("mobile", "Mobile")
        };
    }
}
