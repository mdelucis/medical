﻿using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    public class unsignedInt
    {
        readonly int _value;
        public unsignedInt(int value)
        {
            if (value < 0)
            {
                Error er = new Error();
                er.Type = ErrorType.UnsignedIntLessThanZero;
                er.message = ErrorType.UnsignedIntLessThanZero.GetDescription();

                throw new FhirException(er);
            }

            if (value > 2147483647)
            {
                Error er = new Error();
                er.Type = ErrorType.IntTooBig;
                er.message = ErrorType.IntTooBig.GetDescription();

                throw new FhirException(er);
            }
            this._value = value;
        }
        public static implicit operator int(unsignedInt d)
        {
            return d._value;
        }
        public static implicit operator unsignedInt(int d)
        {
            return new unsignedInt(d);
        }
    }
}
