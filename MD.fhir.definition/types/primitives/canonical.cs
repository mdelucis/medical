﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class canonical
    {
        readonly string _value;
        public canonical(string value)
        {
            this._value = value.ToLowerInvariant();
        }
        public static implicit operator string(canonical d)
        {
            return d._value;
        }
        public static implicit operator canonical(string d)
        {
            return new canonical(d);
        }
    }
}
