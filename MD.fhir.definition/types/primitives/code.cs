﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class code
    {
        readonly string _value;
        public code(string value)
        {
            this._value = value;
        }
        public static implicit operator string(code d)
        {
            return d._value;
        }
        public static implicit operator code(string d)
        {
            return new code(d);
        }
    }
}
