﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class url
    {
        readonly string _value;
        public url(string value)
        {
            this._value = value;
        }
        public static implicit operator string(url d)
        {
            return d._value;
        }
        public static implicit operator url(string d)
        {
            return new url(d);
        }
    }
}
