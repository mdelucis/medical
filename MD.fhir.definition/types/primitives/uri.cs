﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class uri
    {
        readonly string _value;
        public uri(string value)
        {
            this._value = value.ToLowerInvariant();
        }
        public static implicit operator string(uri d)
        {
            return d._value;
        }
        public static implicit operator uri(string d)
        {
            return new uri(d);
        }
    }
}
