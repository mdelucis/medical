﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    public class instant
    {
        readonly DateTime _value;
        public instant(DateTime value)
        {
            this._value = value;
        }
        public static implicit operator DateTime(instant d)
        {
            return d._value;
        }
        public static implicit operator instant(DateTime d)
        {
            return new instant(d);
        }
    }
}
