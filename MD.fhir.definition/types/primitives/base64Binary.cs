﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    public class base64Binary
    {
        readonly string _value;
        public base64Binary(string value)
        {
            this._value = value;
        }
        public static implicit operator string(base64Binary d)
        {
            return d._value;
        }
        public static implicit operator base64Binary(string d)
        {
            return new base64Binary(d);
        }
    }
}
