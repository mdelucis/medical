﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class id
    {
        readonly string _value;
        public id(string value)
        {
            this._value = value;
        }
        public static implicit operator string(id d)
        {
            return d._value;
        }
        public static implicit operator id(string d)
        {
            return new id(d);
        }
    }
}
