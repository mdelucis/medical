﻿using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    public class positiveInt
    {
        readonly int _value;
        public positiveInt(int value)
        {
            if (value < 1)
            {
                Error er = new Error();
                er.Type = ErrorType.PositiveIntLessThanOne;
                er.message = ErrorType.PositiveIntLessThanOne.GetDescription();

                throw new FhirException(er);
            }

            if (value > 2147483647)
            {
                Error er = new Error();
                er.Type = ErrorType.IntTooBig;
                er.message = ErrorType.IntTooBig.GetDescription();

                throw new FhirException(er);
            }
            this._value = value;
        }
        public static implicit operator int(positiveInt d)
        {
            return d._value;
        }
        public static implicit operator positiveInt(int d)
        {
            return new positiveInt(d);
        }
    }
}
