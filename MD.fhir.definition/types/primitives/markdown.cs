﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.primitives
{
    /* Uso un operatore implicito perché la classe agisce semplicemente come string */
    public class markdown
    {
        readonly string _value;
        public markdown(string value)
        {
            this._value = value;
        }
        public static implicit operator string(markdown d)
        {
            return d._value;
        }
        public static implicit operator markdown(string d)
        {
            return new markdown(d);
        }
    }
}
