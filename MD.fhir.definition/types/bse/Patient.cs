﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extensions;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.types.resources;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.bse
{
    /// <summary>
    /// https://www.hl7.org/fhir/patient.html
    /// </summary>
    public class Patient : DomainResource, IConstraint
    {
        public Identifier Identifier { get; set; } = null;
        public bool? active { get; set; } = null;
        public List<HumanName> name { get; set; } = new List<HumanName>();
        public List<ContactPoint> telecom { get; set; } = new List<ContactPoint>();
        public code gender { get; set; } = null;
        public DateTime? birthDate { get; set; } = null;
        public bool? deceasedBoolean { get; set; } = null;
        public DateTime? deceasedDateTime { get; set; } = null;
        public List<Address> address { get; set; } = new List<Address>();
        public CodeableConcept maritalStatus { get; set; } = null;
        public bool? multipleBirthBoolean { get; set; } = null;
        public int? multipleBirthInteger { get; set; } = null;
        public List<Attachment> photo { get; set; } = new List<Attachment>();
        public List<Contact> contact { get; set; } = new List<Contact>();
        public List<Communication> communication { get; set; } = new List<Communication>();
        public List<Reference> generalPractitioner { get; set; } = new List<Reference>();
        public Reference managingOrganization { get; set; } = null;
        public List<Link> link { get; set; } = new List<Link>();

        public new List<Error> Validate()
        {
            List<Error> ler = base.Validate();

            if (gender != null)
            {
                int item = AdministrativeGenders.gender.FindIndex(x => x.Key.Equals(gender));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.GenderNotFound,
                        message = ErrorType.GenderNotFound.GetDescription()
                    });
                }
            }

            if (maritalStatus != null)
            {
                int item = MaritalStatuses.status.FindIndex(x => x.Key.Equals(maritalStatus));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.GenderNotFound,
                        message = ErrorType.GenderNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
