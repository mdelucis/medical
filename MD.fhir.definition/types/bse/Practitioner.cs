﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extensions;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.types.resources;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.bse
{
    public class Practitioner: DomainResource
    {
        public List<Identifier> identifier { get; set; } = new List<Identifier>();
        public bool? active { get; set; } = null;
        public List<HumanName> name { get; set; } = new List<HumanName>();
        public List<ContactPoint> telecom { get; set; } = new List<ContactPoint>();
        public List<Address> address { get; set; } = new List<Address>();
        public code gender { get; set; } = null;
        public DateTime? birthDate { get; set; } = null;
        public List<Attachment> photo { get; set; } = new List<Attachment>();
        public List<Qualification> qualification { get; set; } = new List<Qualification>();
        public List<CodeableConcept> communication { get; set; } = new List<CodeableConcept>();

        public new List<Error> Validate()
        {
            List<Error> ler = base.Validate();

            if (gender != null)
            {
                int item = AdministrativeGenders.gender.FindIndex(x => x.Key.Equals(gender));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.GenderNotFound,
                        message = ErrorType.GenderNotFound.GetDescription()
                    });
                }
            }

            if (communication != null)
            {
                int item = Languages.language.FindIndex(x => x.Key.Equals(communication));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LanguageNotFound,
                        message = ErrorType.LanguageNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
