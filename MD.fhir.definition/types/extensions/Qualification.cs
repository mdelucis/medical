﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extensions
{
    public class Qualification : BackboneElement, IConstraint
    {
        public List<Identifier> identifier { get; set; } = new List<Identifier>();
        public CodeableConcept code { get; set; } = null;
        public Period period { get; set; } = null;
        public Reference issuer { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (code != null)
            {
                int item = Qualifications.qualification.FindIndex(x => x.Key.Equals(code));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.QualificationNotFound,
                        message = ErrorType.QualificationNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
