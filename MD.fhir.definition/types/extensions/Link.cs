﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extensions
{
    public class Link : BackboneElement, IConstraint
    {
        public Reference other { get; set; } = null;
        public code type { get; set; }

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (type != null)
            {
                int item = LinkTypes.type.FindIndex(x => x.Key.Equals(type));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LinkTypeNotFound,
                        message = ErrorType.LinkTypeNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
