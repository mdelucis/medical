﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extensions
{
    public class Contact : BackboneElement, IConstraint
    {
        public List<CodeableConcept> relationship { get; set; } = new List<CodeableConcept>();
        public HumanName name { get; set; } = null;
        public List<ContactPoint> telecom { get; set; } = new List<ContactPoint>();
        public Address address { get; set; } = null;
        public code gender { get; set; } = null;
        public Reference organization { get; set; } = null;
        public Period period { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (gender != null)
            {
                int item = AdministrativeGenders.gender.FindIndex(x => x.Key.Equals(gender));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.GenderNotFound,
                        message = ErrorType.GenderNotFound.GetDescription()
                    });
                }
            }

            if (relationship != null)
            {
                int item = ContactRelationships.relationship.FindIndex(x => x.Key.Equals(relationship));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.RelationshipNotFound,
                        message = ErrorType.RelationshipNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
