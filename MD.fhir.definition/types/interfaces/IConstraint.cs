﻿using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.interfaces
{
	interface IConstraint
	{
		public List<Error> Validate();
	}
}
