﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    /* A specialization of type Quantity */
    public class SimpleQuantity : Element, IConstraint
    {
        public double? value { get; set; } = null;
        public code comparator = null; // Forced.
        public string unit { get; set; } = null;
        public uri system { get; set; } = null;
        public code code { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
