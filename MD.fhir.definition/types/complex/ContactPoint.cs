﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class ContactPoint : Element, IConstraint
    {
        public code system { get; set; } = null;
        public string value { get; set; } = null;
        public code use { get; set; } = null;
        public positiveInt rank { get; set; } = null;
        public Period period { get; set; } = null; 

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (value != null && system is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.ValueWithoutSystem,
                    message = ErrorType.ValueWithoutSystem.GetDescription()
                });
            }

            if (use != null)
            {
                int item = ContactPointSystems.system.FindIndex(x => x.Key.Equals(system));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.ContactPointSystemNotFound,
                        message = ErrorType.ContactPointSystemNotFound.GetDescription()
                    });
                }
            }

            if (use != null)
            {
                int item = ContactPointUses.use.FindIndex(x => x.Key.Equals(use));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.ContactPointUseNotFound,
                        message = ErrorType.ContactPointUseNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
