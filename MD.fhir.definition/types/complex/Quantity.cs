﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Quantity : Element, IConstraint
    {
        public double? value { get; set; } = null;
        public code comparator { get; set; } = null;
        public string unit { get; set; } = null;
        public uri system { get; set; } = null;
        public code code { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();
            Error er = new Error();
            er.Type = ErrorType.NoError;
            er.message = ErrorType.NoError.GetDescription();

            if (code != null && system is null)
            {
                er.Type = ErrorType.QuantityWithUnitWithoutSystem;
                er.message = ErrorType.QuantityWithUnitWithoutSystem.GetDescription();
                ler.Add(er);
            }

            if (comparator != null)
            {
                int item = QuantityComparators.comparator.FindIndex(x => x.Key.Equals(comparator));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.QuantityComparatorNotFound,
                        message = ErrorType.QuantityComparatorNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
