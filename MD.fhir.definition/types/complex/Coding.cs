﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Coding : Element, IConstraint
    {
        public uri system { get; set; } = null;
        public string version { get; set; } = null;
        public code code { get; set; } = null;
        public string display { get; set; } = null;
        public bool? userSelected { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
