﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Ratio : Element, IConstraint
    {
        public Quantity numerator { get; set; } = null;
        public Quantity denominator { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if ((numerator != null && denominator is null) || (numerator is null && denominator != null))
            {                
                ler.Add(new Error
                {
                    Type = ErrorType.RatioNotValid,
                    message = ErrorType.RatioNotValid.GetDescription()
                });                
            }

            //TODO: If both numerator and denominator are absent, there SHALL be some extension present.

            return ler;
        }
    }
}
