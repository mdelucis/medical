﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class CodeableConcept : Element, IConstraint
    {
        public List<Coding> coding { get; set; } = null;
        public string text { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
