﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Period : Element, IConstraint
    {
        public DateTime? start { get; set; } = null;
        public DateTime? end { get; set; } = null;

        public List<Error> Validate()
        {       
            List<Error> ler = new List<Error>();             

            return ler;           
        }
    }
}
