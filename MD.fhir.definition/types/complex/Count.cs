﻿using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Count : Quantity
    {
        public new List<Error> Validate()
        {
            List<Error> ler = base.Validate();

            Error er = new Error();
            er.Type = ErrorType.NoError;
            er.message = ErrorType.NoError.GetDescription();

            if (system != null && !system.ToString().ToUpper().Contains("UCUM"))
            {
                er.Type = ErrorType.UCUMSystemRequired;
                er.message = ErrorType.UCUMSystemRequired.GetDescription();
                ler.Add(er);
            }

            if (code != null && !code.Equals("1"))
            {
                er.Type = ErrorType.CodeValueNotOne;
                er.message = ErrorType.CodeValueNotOne.GetDescription();
                ler.Add(er);
            }

            if (value != null && ((value % 1) != 0))
            {
                er.Type = ErrorType.NotIntegerValueError;
                er.message = ErrorType.NotIntegerValueError.GetDescription();
                ler.Add(er);
            }

            return ler;
        }
    }
}

