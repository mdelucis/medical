﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class HumanName : Element, IConstraint
    {
        public code use { get; set; }
        public string text { get; set; }
        public string family { get; set; }
        public List<string> given { get; set; }
        public List<string> prefix { get; set; }
        public List<string> suffix { get; set; }
        public Period period { get; set; }

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (use != null)
            {
                int item = NameUses.use.FindIndex(x => x.Key.Equals(use));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.NameUseNotFound,
                        message = ErrorType.NameUseNotFound.GetDescription()
                    });
                }
            }           

            return ler;
        }
    }
}
