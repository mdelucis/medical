﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class SampleData : Element, IConstraint
    {
        public SimpleQuantity origin { get; set; } = null;
        public double? period { get; set; } = null;
        public double? factor { get; set; } = null;
        public double? lowerLimit { get; set; } = null;
        public double? upperLimit { get; set; } = null;
        public positiveInt dimensions { get; set; } = null;
        public string data { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
