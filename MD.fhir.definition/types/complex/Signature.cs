﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Signature : Element, IConstraint
    {
        public Coding type { get; set; } = null;
        public instant when { get; set; } = null;
        public Reference who { get; set; } = null;
        public Reference onBehalfOf { get; set; } = null;
        public code targetFormat { get; set; } = null;
        public code sigFormat { get; set; } = null;
        public base64Binary data { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
