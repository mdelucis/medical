﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    /* A specialization of type Quantity */
    public class Duration : Element, IConstraint
    {
        public double? value { get; set; } = null;
        public code comparator { get; set; } = null;
        public string unit { get; set; } = null;
        public uri system { get; set; } = null;
        public code code { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();            

            if (code != null && system is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.QuantityWithUnitWithoutSystem,
                    message = ErrorType.QuantityWithUnitWithoutSystem.GetDescription()
                });                
            }

            if (comparator != null)
            {
                int item = QuantityComparators.comparator.FindIndex(x => x.Key.Equals(comparator));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.QuantityComparatorNotFound,
                        message = ErrorType.QuantityComparatorNotFound.GetDescription()
                    });
                }
            }

            if (value != null)
            {
                int item = DurationUnits.unit.FindIndex(x => x.Key.Equals(code));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.DurationUnitNotFound,
                        message = ErrorType.DurationUnitNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
