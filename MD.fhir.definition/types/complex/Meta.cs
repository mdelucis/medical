﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    /// <summary>
    /// https://www.hl7.org/fhir/resource.html#Meta
    /// </summary>
    public class Meta : Element, IConstraint
    {
        public id versionId { get; set; } = null;
        public instant lastUpdated { get; set; } = null;
        public uri source { get; set; } = null;
        public List<canonical> profile { get; set; } = new List<canonical>();
        public List<Coding> security { get; set; } = new List<Coding>();
        public List<Coding> tag { get; set; } = new List<Coding>();

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
