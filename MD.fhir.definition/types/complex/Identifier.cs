﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Identifier : Element, IConstraint
    {
        public code use { get; set; }
        public CodeableConcept type { get; set; }
        public uri system { get; set; }
        public string value { get; set; }
        public Period period { get; set; }
        public Reference assigner { get; set; }

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (use != null)
            {
                int item = IdentifierUses.use.FindIndex(x => x.Key.Equals(use));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.IdentifierUseNotFound,
                        message = ErrorType.IdentifierUseNotFound.GetDescription()
                    });
                }
            }

            if (type != null)
            {
                int item = IdentifierTypes.type.FindIndex(x => x.Key.Equals(type.coding));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.IdentifierTypeNotFound,
                        message = ErrorType.IdentifierTypeNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
