﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class UsageContext : Element, IConstraint
    {
        public Coding code { get; set; } = null;
        public CodeableConcept valueCodeableConcept { get; set; } = null;
        public Quantity valueQuantity { get; set; } = null;
        public Range valueRange { get; set; } = null;
        public Reference valueReference { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (code != null)
            {
                int item = UsageContextTypes.type.FindIndex(x => x.Key.Equals(code));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.UsaeContextTypeNotFound,
                        message = ErrorType.UsaeContextTypeNotFound.GetDescription()
                    });
                }
            }

            //TODO: Come gestire il value?

            return ler;
        }
    }
}
