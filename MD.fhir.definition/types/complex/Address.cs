﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Address : Element, IConstraint
    {
        public code use { get; set; }
        public code type { get; set; }
        public string text { get; set; }
        public List<string> line { get; set; }
        public string city { get; set; }
        public string district { get; set; }
        public string state { get; set; }
        public string postalCode { get; set; }
        public string country { get; set; }
        public Period period { get; set; }

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (use != null)
            {
                int item = AddressUses.use.FindIndex(x => x.Key.Equals(use));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.AddressUseNotFound,
                        message = ErrorType.AddressUseNotFound.GetDescription()
                    });
                }
            }

            if (type != null)
            {
                int item = AddressTypes.type.FindIndex(x => x.Key.Equals(type));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.AddressTypeNotFound,
                        message = ErrorType.AddressTypeNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
