﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Annotation : Element, IConstraint
    {
        public Reference authorReference { get; set; } = null;
        public string authorString { get; set; } = null;
        public DateTime? time { get; set; } = null;
        public markdown text { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
