﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Attachment : Element, IConstraint
    {
        public List<code> contentType { get; set; } = new List<code>();
        public List<code> language { get; set; } = new List<code>();
        public base64Binary data { get; set; } = null;
        public url url { get; set; } = null;
        public unsignedInt size { get; set; } = null;
        public base64Binary hash { get; set; } = null;
        public string title { get; set; } = null;
        public DateTime? creation { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();
            Error er = new Error();
            er.Type = ErrorType.NoError;
            er.message = ErrorType.NoError.GetDescription();

            if (data != null && contentType.Count == 0)
            {
                er.Type = ErrorType.AttachmentWithDataWithoutContentType;
                er.message = ErrorType.AttachmentWithDataWithoutContentType.GetDescription();
            }

            ler.Add(er);

            if (language != null)
            {
                int item = Languages.language.FindIndex(x => x.Key.Equals(language));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LanguageNotFound,
                        message = ErrorType.LanguageNotFound.GetDescription()
                    });
                }
            }

            //TODO: Add control to mime type

            return ler;
        }
    }

}
