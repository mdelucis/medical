﻿using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Age : Quantity
    {
        public new List<Error> Validate()
        {
            List<Error> ler = base.Validate();

            Error er = new Error();
            er.Type = ErrorType.NoError;
            er.message = ErrorType.NoError.GetDescription();

            if (code != null && system is null)
            {
                er.Type = ErrorType.QuantityWithUnitWithoutSystem;
                er.message = ErrorType.QuantityWithUnitWithoutSystem.GetDescription();
                ler.Add(er);
            }
         
            return ler;
        }
    }
}

