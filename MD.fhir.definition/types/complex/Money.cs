﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Money : Element, IConstraint
    {
        public double value { get; set; }
        public code currency { get; set; }
        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            // TODO: Manage currency valueset

            return ler;
        }
    }
}
