﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Timing : BackboneElement, IConstraint
    {
        public List<DateTime> evnt { get; set; } = new List<DateTime>();
        public Duration boundsDuration { get; set; } = null;
        public Range boundsRange { get; set; } = null;
        public Period boundsPeriod { get; set; } = null;
        public positiveInt count { get; set; } = null;
        public positiveInt countMax { get; set; } = null;
        public double? duration { get; set; } = null;
        public double? durationMax { get; set; } = null;
        public code durationUnit { get; set; } = null;
        public positiveInt frequency { get; set; } = null;
        public positiveInt frequencyMax { get; set; } = null;
        public double? period { get; set; } = null;
        public double? periodMax { get; set; } = null;
        public code periodUnit { get; set; } = null;
        public List<code> dayoOfWeek { get; set; } = new List<code>();
        public DateTime? timeOfDay { get; set; } = null; // Only time part used.
        public code when { get; set; } = null;
        public unsignedInt offset { get; set; } = null;
        public CodeableConcept code { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (duration != null && durationUnit is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.DurationWithoutUnit,
                    message = ErrorType.DurationWithoutUnit.GetDescription()
                });
            }

            if (duration < 0)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.DurationNegative,
                    message = ErrorType.DurationNegative.GetDescription()
                });                
            }

            if (durationMax != null && duration is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.DurationMaxWithoutDuration,
                    message = ErrorType.DurationMaxWithoutDuration.GetDescription()
                });
            }

            if (period != null && periodUnit is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.PeriodWithoutUnit,
                    message = ErrorType.PeriodWithoutUnit.GetDescription()
                });
            }

            if (period < 0)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.PeriodNegative,
                    message = ErrorType.PeriodNegative.GetDescription()
                });
            }

            if (periodMax != null && period is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.PeriodMaxWithoutPeriod,
                    message = ErrorType.PeriodMaxWithoutPeriod.GetDescription()
                });
            }

            if (countMax != null && count is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.CountMaxWithoutCount,
                    message = ErrorType.CountMaxWithoutCount.GetDescription()
                });
            }

            if (offset != null && (when is null || when.Equals("C") || when.Equals("CM") || when.Equals("CD") || when.Equals("CV")))
            {
                ler.Add(new Error
                {
                    Type = ErrorType.OffsetWithoutWhen,
                    message = ErrorType.OffsetWithoutWhen.GetDescription()
                });
            }

            if (timeOfDay != null && when != null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.TimeOfDayOrWhen,
                    message = ErrorType.TimeOfDayOrWhen.GetDescription()
                });
            }

            return ler;
        }
    }
}
