﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Range : Element, IConstraint
    {
        public SimpleQuantity low { get; set; } = null;
        public SimpleQuantity high { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (low != null)
            {
                if (low.value > high.value)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LowGreaterThanHigh,
                        message = ErrorType.LowGreaterThanHigh.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
