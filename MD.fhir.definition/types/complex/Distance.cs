﻿using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.complex
{
    public class Distance : Quantity
    {
        public new List<Error> Validate()
        {
            List<Error> ler = base.Validate();

            Error er = new Error();
            er.Type = ErrorType.NoError;
            er.message = ErrorType.NoError.GetDescription();

            if (system != null && !system.ToString().ToUpper().Contains("UCUM"))
            {
                er.Type = ErrorType.UCUMSystemRequired;
                er.message = ErrorType.UCUMSystemRequired.GetDescription();
                ler.Add(er);
            }

            if (code != null && value is null)
            {
                er.Type = ErrorType.ValueWithoutCode;
                er.message = ErrorType.ValueWithoutCode.GetDescription();
                ler.Add(er);
            }

            return ler;
        }
    }
}

