﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extension;
using MD.fhir.definition.types.interfaces;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.resources
{
    /// <summary>
    /// https://www.hl7.org/fhir/domainresource.html
    /// </summary>
    public abstract class DomainResource : Resource, IConstraint
    {
        public Narrative text { get; set; } = null;
        public List<Resource> contained { get; set; } = new List<Resource>();
        public List<Extension> extension { get; set; } = new List<Extension>();
        public List<Extension> modifierExtension { get; set; } = new List<Extension>();

        public new List<Error> Validate()
        {            
            List<Error> ler = base.Validate();

            // TODO: Cercare di implementare la regola dom-3
            
            foreach (var item in contained)
            {
               if (item.GetType() == typeof(DomainResource))
                {
                    DomainResource dr = (DomainResource)item;
                    if (dr.contained.Count > 0)
                    {
                        ler.Add(new Error
                        {
                            Type = ErrorType.ContainedContainedNOTEmpty,
                            message = String.Concat(ErrorType.ContainedContainedNOTEmpty.GetDescription(), $" (id: {dr.id}")
                        });
                    }

                    if (dr.meta.versionId != null || dr.meta.lastUpdated != null)
                    {
                        ler.Add(new Error
                        {
                            Type = ErrorType.ContainedMetaError,
                            message = String.Concat(ErrorType.ContainedMetaError.GetDescription(), $" (id: {dr.id}")
                        });
                    }

                    if (dr.meta.security != null)
                    {
                        ler.Add(new Error
                        {
                            Type = ErrorType.ContainedMetaSecurityError,
                            message = String.Concat(ErrorType.ContainedMetaSecurityError.GetDescription(), $" (id: {dr.id}")
                        });
                    }
                }
            }

            return ler;
        }
    }
}
