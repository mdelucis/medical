﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.metadata;
using MD.fhir.definition.types.primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.resources
{
    /// <summary>
    /// https://www.hl7.org/fhir/structuredefinition.html
    /// </summary>
    public class StructureDefinition : DomainResource
    {
        public uri url { get; set; } = null;
        public List<Identifier> identifier { get; set; } = new List<Identifier>();
        public string version { get; set; } = null;
        public string name { get; set; } = null;
        public string title { get; set; } = null;
        public code status { get; set; } = null;
        public bool? experimental { get; set; } = null;
        public DateTime? date { get; set; } = null;
        public string publisher { get; set; } = null;
        public List<ContactDetail> contact { get; set; } = new List<ContactDetail>();
        public markdown description { get; set; } = null;
        public List<UsageContext> useContext { get; set; } = new List<UsageContext>();
        public List<CodeableConcept> jurisdiction { get; set; } = new List<CodeableConcept>();
        public markdown purpose { get; set; } = null;
        public markdown copyright { get; set; } = null;
        public List<Coding> keyworld { get; set; } = null;
        public code fhirVersion { get; set; } = null;

        //TODO: Completare
    }
}
