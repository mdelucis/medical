﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.resources
{
    public abstract class Resource : IConstraint
    {
        public id id { get; set; } = null;
        public Meta meta { get; set; } = null;
        public uri implicitRules { get; set; } = null;
        public code language { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (language != null)
            {
                int item = Languages.language.FindIndex(x => x.Key.Equals(language));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LanguageNotFound,
                        message = ErrorType.LanguageNotFound.GetDescription()
                    });
                }
            }            

            return ler;
        }
    }
}
