﻿using MD.fhir.definition.types.extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extra
{
    public abstract class BackboneElement : Element
    {
        public Extension modifierExtension { get; set; }
    }
}
