﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extra
{
    public class Reference
    {
        public string reference { get; set; }
        public uri type { get; set; }
        public Identifier identifier { get; set; }
        public string display { get; set; }
    }
}
