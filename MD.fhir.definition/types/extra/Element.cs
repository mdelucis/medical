﻿using MD.fhir.definition.types.extension;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extra
{
    public abstract class Element
    {
        public string id { get; set; }
        public IExtension extension { get; set; }
    }
}
