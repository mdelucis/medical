﻿using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.primitives;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.extension
{
    public class Extension : Element
    {
        public uri url { get; set; }
        public Element value { get; set; }
    }
}
