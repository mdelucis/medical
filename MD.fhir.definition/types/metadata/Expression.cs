﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.metadata
{
    public class Expression : Element, IConstraint
    {
        public string description { get; set; } = null;
        public string name { get; set; } = null;
        public code language { get; set; } = null;
        public string expression { get; set; } = null;
        public uri reference { get; set; } = null;

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (expression is null && reference is null)
            {
                ler.Add(new Error
                {
                    Type = ErrorType.ExpressionOrReferenceRequired,
                    message = ErrorType.ExpressionOrReferenceRequired.GetDescription()
                });                
            }

            if (language != null)
            {
                int item = ExpressionLanguages.language.FindIndex(x => x.Key.Equals(language));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.LanguageNotFound,
                        message = ErrorType.LanguageNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
