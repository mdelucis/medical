﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.support;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.metadata
{
    public class ContactDetail : Element, IConstraint
    {
        public string name { get; set; } = null;
        public List<ContactPoint> telecom { get; set; } = new List<ContactPoint>();

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            return ler;
        }
    }
}
