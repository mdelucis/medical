﻿using MD.fhir.definition.types.complex;
using MD.fhir.definition.types.extra;
using MD.fhir.definition.types.interfaces;
using MD.fhir.definition.types.primitives;
using MD.fhir.definition.valuesets;
using MD.support;
using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.fhir.definition.types.metadata
{
    public class Contributor : Element, IConstraint
    {
        public code type { get; set; } = null;
        public string name { get; set; } = null;
        public List<ContactDetail> contact { get; set; } = new List<ContactDetail>();

        public List<Error> Validate()
        {
            List<Error> ler = new List<Error>();

            if (type != null)
            {
                int item = ContributorTypes.type.FindIndex(x => x.Key.Equals(type));

                if (item < 0)
                {
                    ler.Add(new Error
                    {
                        Type = ErrorType.ContributorTypeNotFound,
                        message = ErrorType.ContributorTypeNotFound.GetDescription()
                    });
                }
            }

            return ler;
        }
    }
}
