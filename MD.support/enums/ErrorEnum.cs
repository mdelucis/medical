﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MD.support.enums
{
	public enum ErrorType
	{
		[Description("OK")]
		NoError = 0,
		[Description("Too many values")]
		TooManyValues = 1,
		/* Error codes between 100 and 199 are reserved for primitive fhir types */
		[Description("A UnsignedInt datum cannot take values less than zero")]
		UnsignedIntLessThanZero = 100,
		[Description("UnsignedInt or positiveInt data cannot be larger than 2.147.483.647")]
		IntTooBig = 101,
		[Description("A positiveInt datum cannot take values less than one")]
		PositiveIntLessThanOne = 102,
		/* Error codes between 200 and 299 are reserved for complex fhir types */
		[Description("If the Attachment has data, it SHALL have a contentType")]
		AttachmentWithDataWithoutContentType = 200,
		[Description("If a code for the unit is present, the system SHALL also be present")]
		QuantityWithUnitWithoutSystem = 201,
		[Description("Language not found")]
		LanguageNotFound = 202,
		[Description("Quantity comparator not found")]
		QuantityComparatorNotFound = 203,
		[Description("Duration unit not found")]
		DurationUnitNotFound = 204,
		[Description("If present, low SHALL have a lower value than high")]
		LowGreaterThanHigh = 205,
		[Description("Numerator and denominator SHALL both be present, or both are absent")]
		RatioNotValid = 206,
		[Description("Identified use not found")]
		IdentifierUseNotFound = 207,
		[Description("Identified type not found")]
		IdentifierTypeNotFound = 208,
		[Description("Name use not found")]
		NameUseNotFound = 209,
		[Description("Address use not found")]
		AddressUseNotFound = 210,
		[Description("Address type not found")]
		AddressTypeNotFound = 211,
		[Description("A system is required if a value is provided")]
		ValueWithoutSystem = 212,
		[Description("Contact point system not found")]
		ContactPointSystemNotFound = 213,
		[Description("Contact point use not found")]
		ContactPointUseNotFound = 214,
		[Description("If there's a duration, there needs to be duration units")]
		DurationWithoutUnit = 215,
		[Description("If there's a period, there needs to be period units")]
		PeriodWithoutUnit = 216,
		[Description("Duration SHALL be a non-negative value")]
		DurationNegative = 217,
		[Description("Period SHALL be a non-negative value")]
		PeriodNegative = 218,
		[Description("If there's a periodMax, there must be a period")]
		PeriodMaxWithoutPeriod = 219,
		[Description("If there's a durationMax, there must be a duration")]
		DurationMaxWithoutDuration = 220,
		[Description("If there's a countMax, there must be a count")]
		CountMaxWithoutCount = 221,
		[Description("If there's an offset, there must be a when (and not C, CM, CD, CV)")]
		OffsetWithoutWhen = 222,
		[Description("If there's a timeOfDay, there cannot be a when, or vice versa")]
		TimeOfDayOrWhen = 223,
		[Description("Usage context type not found")]
		UsaeContextTypeNotFound = 224,
		[Description("If system is present, it SHALL be UCUM")]
		UCUMSystemRequired = 225,
		[Description("There SHALL be a code with a value of \"1\" if there is a value")]
		CodeValueNotOne = 226,
		[Description("If present, the value SHALL be a whole number")]
		NotIntegerValueError = 227, 
		[Description("There SHALL be a code if there is a value")]
		ValueWithoutCode = 228,
		[Description("Contributor type not found")]
		ContributorTypeNotFound = 229,
		[Description("An expression or a reference must be provided")]
		ExpressionOrReferenceRequired = 230,
		/* Error codes between 300 and 399 are reserved for resource fhir types */
		[Description("If the resource is contained in another resource, it SHALL NOT contain nested Resources")]
		ContainedContainedNOTEmpty = 300,
		[Description("If the resource is contained in another resource, it SHALL be referred to from elsewhere in the resource or SHALL refer to the containing resource")]
		ContainedNotReferred = 301,
		[Description("If a resource is contained in another resource, it SHALL NOT have a meta.versionId or a meta.lastUpdated")]
		ContainedMetaError = 302,
		[Description("If a resource is contained in another resource, it SHALL NOT have a security label")]
		ContainedMetaSecurityError = 303,
		/* Error codes between 400 and 499 are reserved for base fhir types (Patient, etc.) */
		[Description("Gender not found")]
		GenderNotFound = 400,
		[Description("Marital status not found")]
		MaritalStatusNotFound = 401,
		[Description("Relationship not found")]
		RelationshipNotFound = 402,
		[Description("Link Type not found")]
		LinkTypeNotFound = 403,
		[Description("Qualification not found")]
		QualificationNotFound = 404
	}
}
