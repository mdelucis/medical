﻿using MD.support.enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace MD.support
{
    public class Error
    {
        public ErrorType Type { get; set; }
        public string message { get; set; }
    }

    public class FhirException : Exception
    {
        public FhirException(Error er)
           : base(er.message)
        {
            base.HResult = (int)er.Type;
        }
    }
}
